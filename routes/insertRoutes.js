const middleware = require("../middleware"),
      express    = require("express"),
      router     = express.Router();

const db = require('../models/indexDB.js');

router.get("/", middleware.isLoggedIn, (req, res) => {
  res.render("insert/indexInsert");
});

router.post("/movie", middleware.isLoggedIn, (req, res) => {

  var query = '';

  if (req.body.colleccio === '') {
    query = "insert into movie values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", null, " + req.body.nota;
  } else {
    query = "insert into movie values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", $$" + req.body.colleccio + "$$, " + req.body.nota;
  }

  if (req.body.imdbcode !== '') {
    query = query + ", $$" + req.body.imdbcode + "$$);";
  } else {
    query = query + ", null);";
  }

  var dropMovie = "delete from previstes where imdbcode = \'" + req.body.imdbcode + "\'"

  db
    .query(dropMovie)
    .then(resQ => {
      //empty
    })
    .catch(err => {
      req.flash("error", err.message);
      return res.redirect("/select");
    })

  db.query(query, (err, resQ) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }
    req.flash("success", "Camp inserit correctament");
    res.redirect("/select");
  })

});

router.post("/tv", middleware.isLoggedIn, (req, res) => {

  var query = '';

  if (req.body.colleccio === '') {
    if (req.body.date_fi === '') {
      query = "insert into tv_show values($$" + req.body.titol + "$$, $$" + req.body.creador + "$$, " + req.body.date_inici + ", null, " + req.body.temporades + ", null, $$" + req.body.nota + "$$";
    } else {
      query = "insert into tv_show values($$" + req.body.titol + "$$, $$" + req.body.creador + "$$, " + req.body.date_inici + ", " + req.body.date_fi + ", " + req.body.temporades + ", null, $$" + req.body.nota + "$$";
    }

  } else {
    if (req.body.date_fi === '') {
      query = "insert into tv_show values($$" + req.body.titol + "$$, $$" + req.body.creador + "$$, " + req.body.date_inici + ", null, " + req.body.temporades + ", $$" + req.body.colleccio + "$$, $$" + req.body.nota + "$$";
    } else {
      query = "insert into tv_show values($$" + req.body.titol + "$$, $$" + req.body.creador + "$$, " + req.body.date_inici + ", " + req.body.date_fi + ", " + req.body.temporades + ", $$" + req.body.colleccio + "$$, $$" + req.body.nota + "$$";
    }
  }

  if (req.body.imdbcode !== '') {
    query = query + ", $$" + req.body.imdbcode + "$$);";
  } else {
    query = query + ", null);";
  }

  var dropTv = "delete from previstes where imdbcode = \'" + req.body.imdbcode + "\'"

  db
    .query(dropTv)
    .then(resQ => {
      //empty
    })
    .catch(err => {
      req.flash("error", err.message);
      return res.redirect("/select");
    })

  db.query(query, (err, resQ) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }
    req.flash("success", "Camp inserit correctament");
    res.redirect("/select");
  })
});

router.post("/prev", middleware.isLoggedIn, (req, res) => {

  var query = '';

  if (req.body.colleccio === '') {
    if (req.body.temporades === '') {
      query = "insert into previstes values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", null, null";
    } else {
      query = "insert into previstes values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", " + req.body.temporades + ", null";
    }

  } else {
    if (req.body.temporades === '') {
      query = "insert into previstes values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", null, $$" + req.body.colleccio + "$$";
    } else {
      query = "insert into previstes values($$" + req.body.titol + "$$, $$" + req.body.director + "$$, " + req.body.date_estrena + ", " + req.body.temporades + ", $$" + req.body.colleccio + "$$";
    }
  }

  if (req.body.imdbcode !== '') {
    query = query + ", $$" + req.body.imdbcode + "$$);";
  } else {
    query = query + ", null);";
  }

  db.query(query, (err, resQ) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }
    req.flash("success", "Camp inserit correctament");
    res.redirect("back");
  })
});

router.get("/:id/movie", (req, res) => {

  var query = "select * from previstes where imdbcode = '" + req.params.id + "';"
  var movie = '';

  db
    .query(query)
    .then(resQ => {
      movie = resQ.rows[0];
      if (movie.length == 0) {
        req.flash("err", err.message);
      } else {
        res.render("insert/toMovie", {
          movie: movie
        });
      }
    })
    .catch(err => {
      req.flash("err", err.message);
    })

});

router.get("/:id/tv", (req, res) => {

  var query = "select * from previstes where imdbcode = '" + req.params.id + "';"
  var tv = '';

  db
    .query(query)
    .then(resQ => {
      tv = resQ.rows[0];
      if (tv.length == 0) {
        req.flash("err", err.message);
      } else {
        res.render("insert/toTv", {
          tv: tv
        });
      }
    })
    .catch(err => {
      req.flash("err", err.message);
    })

});

module.exports = router;
