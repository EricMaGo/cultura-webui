const express = require("express"),
      router  = express.Router(),
      bent    = require('bent'); //http to get json

const db = require('../models/indexDB.js');
const getJSON = bent('json'); //http to get json

var passport = require("passport"),
    User     = require("../models/user"),
    middleware = require("../middleware");

router.get("/", (req, res) => {
  res.render("landing");
});

router.get("/imdbcheck/:id", (req, res) => {

  imdb()

  async function imdb() {
    try {
      var responseObject = await getJSON('https://imdb-api.com/en/API/Title/'+ process.env.APIKEY +'/' + req.params.id)
      var request = {
        id: req.params.id,
        image: responseObject.image,
        imDbRating: responseObject.imDbRating,
        fullTitle: responseObject.fullTitle,
        stars: responseObject.stars,
        genres: responseObject.genres,
        plot: responseObject.plot,
      }

      try {
        request.creators = responseObject.tvSeriesInfo.creators
      } catch {
        request.directors = responseObject.directors
      } finally {
        res.send(request)
      }

    } catch (err) {
      res.send(["error", err])
    } finally {
      //do nothing
    }

  }
})

router.get("/del/:from/:id", middleware.isLoggedIn, (req, res) => {

  const query = "delete from " + req.params.from + " where imdbcode = \'" + req.params.id + "\';"

  db
    .query(query)
    .then(resQ => {
      if (resQ.rowCount == 0) {
        req.flash("error", "No s'ha trobat cap coincidencia")
        res.redirect("/select");
      } else if (resQ.rowCount === 1) {
        req.flash("success", "Un camp eliminat correctament")
        res.redirect("/select");
      } else {
        req.flash("success", resQ.rowCount + " camps eliminats correctament")
        res.redirect("/select");
      }

    })
    .catch(err => {
      req.flash("error", err.message)
      res.redirect("/select");
    })

})

router.get("/login", (req, res) => {
  res.render("login", {page: 'login'});
});

// router.get("/register", (req, res) => {
//   res.render("register", {page: 'register'});
// });
//
// router.post("/register", (req, res) => {
//   var newUser = new User({
//     username: req.body.username,
//   });
//
//   User.register(newUser, req.body.password, (err, user) => {
//     if (err) {
//       return res.render("register", {"error": err.message});
//     }
//     passport.authenticate("local")(req, res, () => {
//       req.flash("success", "Welcome, " + user.username);
//       res.redirect("/select");
//     });
//   });
// });

router.post("/login", passport.authenticate("local", {
  successRedirect: "/inside",
  failureRedirect: "/login",
  failureFlash: true,
  successFlash: "Benvingut!",
}), (req, res) => {
  //empty
})

router.get("/inside", (req, res) => {
  res.render("inside");
})

router.get("/logout", (req, res) => {
  req.logout();
  req.flash("success", "Sessió tancada correctament. Fins aviat!");
  res.redirect("/select");
});

module.exports = router;
