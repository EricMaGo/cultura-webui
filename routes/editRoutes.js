const middleware = require("../middleware"),
      express = require("express");
      router = express.Router();

const db = require('../models/indexDB.js');

router.get("/movie/:id", middleware.isLoggedIn, (req, res) => {

  var query = "select * from movie where imdbcode = '" + req.params.id + "';"
  var movie = '';

  db
    .query(query)
    .then(resQ => {
      movie = resQ.rows[0];
      if (movie.length == 0) {
        req.flash("err", err.message);
      } else {
        res.render("edit/movieEdit", {
          movie: movie
        });
      }
    })
    .catch(err => {
      req.flash("err", err.message);
    })

});

router.get("/tv/:id", middleware.isLoggedIn, (req, res) => {

  var query = "select * from tv_show where imdbcode = '" + req.params.id + "';"
  var tv = '';

  db
    .query(query)
    .then(resQ => {
      tv = resQ.rows[0];
      if (tv.length == 0) {
        req.flash("err", err.message);
      } else {
        res.render("edit/tvEdit", {
          tv: tv
        });
      }
    })
    .catch(err => {
      req.flash("err", err.message);
    })

});

router.get("/prev/:id", middleware.isLoggedIn, (req, res) => {

  var query = "select * from previstes where imdbcode = '" + req.params.id + "';"
  var movie = '';

  db
    .query(query)
    .then(resQ => {
      prev = resQ.rows[0];
      if (prev.length == 0) {
        req.flash("err", err.message);
      } else {
        res.render("edit/prevEdit", {
          prev: prev
        });
      }
    })
    .catch(err => {
      req.flash("err", err.message);
    })

});

router.post("/movie/:id", middleware.isLoggedIn, (req, res) => {

  var query = '';
  var newValues = '';

  function andWhere() {
    if (newValues !== '') {
      newValues = newValues + ', '
    }
  }

  if (req.body.titol !== '') {
    newValues = newValues + "titol = \'" + req.body.titol + "\'"
  }

  if (req.body.director !== '') {
    andWhere();
    newValues = newValues + "director = \'" + req.body.director + "\'"
  }

  if (req.body.date_estrena !== '') {
    andWhere();
    newValues = newValues + "any_publicació = \'" + req.body.date_estrena + "\'"
  }

  if (req.body.colleccio !== '') {
    andWhere();
    newValues = newValues + "col·lecció = \'" + req.body.colleccio + "\'"
  }

  if (req.body.nota !== '') {
    andWhere();
    newValues = newValues + "rate = \'" + req.body.nota + "\'"
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    newValues = newValues + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  var query = "update movie set " + newValues + " where imdbcode = \'" + req.params.id + "\';"

  db.query(query, (err, resQ) => {
    if (err) {
      console.log(err);
      return;
    }
    req.flash("success", "Camp actualitzat correctament");
    res.redirect("/select/");
  });

});

router.post("/tv/:id", middleware.isLoggedIn, (req, res) => {

  var query = '';
  var newValues = '';

  function andWhere() {
    if (newValues !== '') {
      newValues = newValues + ', '
    }
  }

  if (req.body.titol !== '') {
    newValues = newValues + "titol = \'" + req.body.titol + "\'"
  }

  if (req.body.creador !== '') {
    andWhere();
    newValues = newValues + "creador = \'" + req.body.creador + "\'"
  }

  if (req.body.date_inici !== '') {
    andWhere();
    newValues = newValues + "any_inici = \'" + req.body.date_inici + "\'"
  }

  if (req.body.date_fi !== '') {
    andWhere();
    newValues = newValues + "any_fi = \'" + req.body.date_fi + "\'"
  }

  if (req.body.temporades !== '') {
    andWhere();
    newValues = newValues + "temporades = \'" + req.body.temporades + "\'"
  }

  if (req.body.colleccio !== '') {
    andWhere();
    newValues = newValues + "col·lecció = \'" + req.body.colleccio + "\'"
  }

  if (req.body.nota !== '') {
    andWhere();
    newValues = newValues + "rate = \'" + req.body.nota + "\'"
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    newValues = newValues + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  var query = "update tv_show set " + newValues + " where imdbcode = \'" + req.params.id + "\';"

  db.query(query, (err, resQ) => {
    if (err) {
      console.log(err);
      return;
    }
    req.flash("success", "Camp actualitzat correctament");
    res.redirect("/select/");
  });

});

router.post("/prev/:id", middleware.isLoggedIn, (req, res) => {

  var query = '';
  var newValues = '';

  function andWhere() {
    if (newValues !== '') {
      newValues = newValues + ', '
    }
  }

  if (req.body.titol !== '') {
    newValues = newValues + "titol = \'" + req.body.titol + "\'"
  }

  if (req.body.director_creador !== '') {
    andWhere();
    newValues = newValues + "director_creador = \'" + req.body.director_creador + "\'"
  }

  if (req.body.date_estrena !== '') {
    andWhere();
    newValues = newValues + "any_publicació = \'" + req.body.date_estrena + "\'"
  }

  if (req.body.temporades !== '') {
    andWhere();
    newValues = newValues + "temporades = \'" + req.body.temporades + "\'"
  }

  if (req.body.colleccio !== '') {
    andWhere();
    newValues = newValues + "col·lecció = \'" + req.body.colleccio + "\'"
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    newValues = newValues + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  var query = "update previstes set " + newValues + " where imdbcode = \'" + req.params.id + "\';"

  db.query(query, (err, resQ) => {
    if (err) {
      console.log(err);
      return;
    }
    req.flash("success", "Camp actualitzat correctament");
    res.redirect("/select/");
  });
});

module.exports = router;
