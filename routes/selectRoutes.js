const express = require("express"),
      router = express.Router();

const db = require('../models/indexDB.js');

router.get("/", (req, res) => {
  res.render("select/indexSelect");
});

router.post("/movie", (req, res) => {

  if (req.body.titol === '' && req.body.director === '' && req.body.date_estrena === '' && req.body.colleccio === '' && req.body.imdbcode === '' && !req.body.nota) {
    req.flash("error", "Introdueix alguna dada per realitzar la cerca")
    return res.redirect("back");
  }

  var where = '';

  function andWhere() {
    if (where !== '') {
      where = where + ' AND '
    }
  }

  if (req.body.titol !== '') {
    where = where + "lower(titol) like \'%" + req.body.titol.toLowerCase() + "%\'"
  }

  if (req.body.director !== '') {
    andWhere();
    where = where + "lower(director) like \'%" + req.body.director.toLowerCase() + "%\'"
  }

  if (req.body.date_estrena !== '') {
    andWhere();
    where = where + "any_publicació = " + req.body.date_estrena
  }

  if (req.body.colleccio !== '') {
    andWhere();
    where = where + "lower(col·lecció) like \'%" + req.body.colleccio.toLowerCase() + "%\'"
  }

  if (req.body.nota) {
    andWhere();
    where = where + "rate = \'" + req.body.nota + "\'"
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    where = where + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  const query = "select * from movie where " + where + " order by " + req.body.order + " " + req.body.order_type + ";";
  var movie = '';

  db
    .query(query)
    .then(resQ => {
      movie = resQ.rows;
      if (movie.length == 0) {
        req.flash("error", "Cap camp amb les dades aportades");
        res.redirect("back");
      } else {
        res.render("select/movieSelect", {
          movie: movie
        });
      }
    })
    .catch(err => {
      req.flash('error', err.message);
      res.redirect("back");
    })

});

router.post("/tv", (req, res) => {

  if (req.body.titol === '' && req.body.creador === '' && req.body.date_inici === '' && req.body.date_fi === '' && req.body.temporades === '' && req.body.colleccio === '' && req.body.imdbcode === '' && !req.body.nota) {
    req.flash("error", "Introdueix alguna dada per realitzar la cerca")
    return res.redirect("back");
  }

  var where = '';

  function andWhere() {
    if (where !== '') {
      where = where + ' AND '
    }
  }

  if (req.body.titol !== '') {
    where = where + "lower(titol) like \'%" + req.body.titol.toLowerCase() + "%\'"
  }

  if (req.body.creador !== '') {
    andWhere();
    where = where + "lower(creador) like \'%" + req.body.creador.toLowerCase() + "%\'"
  }

  if (req.body.date_inici !== '') {
    andWhere();
    where = where + "any_inici = " + req.body.date_inici
  }

  if (req.body.date_fi !== '') {
    andWhere();
    where = where + "any_fi = " + req.body.date_fi
  }

  if (req.body.temporades !== '') {
    andWhere();
    where = where + "temporades = \'" + req.body.temporades + "\'"
  }

  if (req.body.colleccio !== '') {
    andWhere();
    where = where + "lower(col·lecció) like \'%" + req.body.colleccio.toLowerCase() + "%\'"
  }

  if (req.body.nota) {
    andWhere();
    where = where + "rate = \'" + req.body.nota + "\'"
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    where = where + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  const query = "select * from tv_show where " + where + " order by " + req.body.order + " " + req.body.order_type + ";";
  var tv = "";

  db
    .query(query)
    .then(resQ => {
      tv = resQ.rows;
      if (tv.length == 0) {
        req.flash("error", "Cap camp amb les dades aportades");
        res.redirect("back");
      } else {
        res.render("select/tvSelect", {
          tv: tv
        });
      }
    })
    .catch(err => {
      req.flash('error', err.message);
      res.redirect("back");
    })
});

router.post("/prev", (req, res) => {

  if (req.body.titol === '' && req.body.director === '' && req.body.date_estrena === '' && req.body.colleccio === '' && req.body.imdbcode === '' && req.body.temporades === '') {
    req.flash("error", "Introdueix alguna dada per realitzar la cerca")
    return res.redirect("back");
  }

  var where = '';

  function andWhere() {
    if (where !== '') {
      where = where + ' AND '
    }
  }

  if (req.body.titol !== '') {
    where = where + "lower(titol) like \'%" + req.body.titol.toLowerCase() + "%\'";
  }

  if (req.body.director !== '') {
    andWhere();
    where = where + "lower(director_creador) like \'%" + req.body.director.toLowerCase() + "%\'";
  }

  if (req.body.date_estrena !== '') {
    andWhere();
    where = where + "any_publicació = " + req.body.date_estrena;
  }

  if (req.body.temporades !== '') {
    andWhere();
    where = where + "temporades = " + req.body.temporades;
  }

  if (req.body.colleccio !== '') {
    andWhere();
    where = where + "lower(col·lecció) like \'%" + req.body.colleccio.toLowerCase() + "%\'";
  }

  if (req.body.imdbcode !== '') {
    andWhere();
    where = where + "imdbcode = \'" + req.body.imdbcode + "\'"
  }

  const query = "select * from previstes where " + where + " order by " + req.body.order + " " + req.body.order_type + ";";
  var prev = "";

  db
    .query(query)
    .then(resQ => {
      prev = resQ.rows;
      if (prev.length == 0) {
        req.flash("error", "Cap camp amb les dades aportades");
        res.redirect("back");
      } else {
        res.render("select/prevSelect", {
          prev: prev
        });
      }
    })
    .catch(err => {
      req.flash('error', err.message);
      res.redirect("back");
    })
});

router.get("/:from/all", (req, res) => {

  if (req.params.from == 'movie') {

    var movie = '';
    const query = "select * from movie order by titol;"

    db
      .query(query)
      .then(resQ => {
        movie = resQ.rows;
        if (movie.length == 0) {
          req.flash("error", "Cap camp amb les dades aportades");
          res.redirect("back");
        } else {
          res.render("select/movieSelect", {
            movie: movie
          });
        }
      })
      .catch(err => {
        req.flash('error', err.message);
        res.redirect("back");
      })

  } else if (req.params.from == 'tv') {

    var tv = ''
    const query = "select * from tv_show order by titol;"

    db
      .query(query)
      .then(resQ => {
        tv = resQ.rows;
        if (tv.length == 0) {
          req.flash("error", "Cap camp amb les dades aportades");
          res.redirect("back");
        } else {
          res.render("select/tvSelect", {
            tv: tv
          });
        }
      })
      .catch(err => {
        req.flash('error', err.message);
        res.redirect("back");
      })

  } else if (req.params.from == 'prev') {

    var prev = ''
    const query = "select * from previstes order by titol;"

    db
      .query(query)
      .then(resQ => {
        prev = resQ.rows;
        if (prev.length == 0) {
          req.flash("error", "Cap camp amb les dades aportades");
          res.redirect("back");
        } else {
          res.render("select/prevSelect", {
            prev: prev
          });
        }
      })
      .catch(err => {
        req.flash('error', err.message);
        res.redirect("back");
      })

  }

})

module.exports = router;
