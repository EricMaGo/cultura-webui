var imdb = '';

$(".checkIMDb").on("click", function() {
  $(".checkIMDb").removeClass("btn-warning")
  $(".checkIMDb").removeClass("btn-danger")
  $(".checkIMDb").addClass("btn-warning")
  $("i").removeClass("bi-lock")
  $("i").addClass("bi-hourglass-split")
  $("#error").remove()
  $("#success").remove()
  imdb = $('[name="imdbcode"]').val();
  if (!imdb) {
    imdb = $('[name="imdbcode"]')[0].value
  }
  if (!imdb) {
    imdb = $('[name="imdbcode"]')[1].value
  }
  if (!imdb) {
    imdb = $('[name="imdbcode"]')[2].value
  }
  ifExists(imdb)
})

function ifExists(imdb) {
  $.ajax({
    type: 'GET',
    url: '/imdbcheck/' + imdb,
    timeout: '10000',
    success: function(data) {
      if (data[0] === "error") {
        alert("Ho sentim, però ens hem trobat un error. Si vols pots provar-ho més tard\r\n\t\t\t\t\tError: " + data[1].message);
      } else {
        if (data.fullTitle) {
          $(".checkIMDb").removeClass("btn-warning")
          $(".checkIMDb").removeClass("btn-danger")
          $("#error").remove()
          $(".checkIMDb").before("<p id=\"success\" style=\"color: #5cb85c\"><em>Codi correcte</em></p>")
          $(".checkIMDb").addClass("btn-success")
          $("i").removeClass("bi-hourglass-split")
          $("i").addClass("bi-check-all")
          $(".submit").removeAttr("disabled")
        } else {
          $(".checkIMDb").removeClass("btn-success")
          $(".checkIMDb").addClass("btn-danger")
          $(".checkIMDb").before("<p id=\"error\" style=\"color: red\"><em>Codi erroni. Comprova i torna a provar</em></p>")
          $("i").removeClass("bi-hourglass-split")
          $("i").addClass("bi-arrow-repeat")
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
  });
}
