function deactiveAll() {
  var formDisplay = document.querySelectorAll("form");
  var tabDisplay = document.querySelectorAll(".tab-form");

  for (var i = 0; i < formDisplay.length; i++) {
    var atr = formDisplay[i].getAttribute("class");
    var atr = atr + " form-deactive"
    var atr = atr.replace("form-active","");
    formDisplay[i].setAttribute("class", atr);

    var atr = tabDisplay[i].getAttribute("class");
    atr = atr.replace('active','');
    tabDisplay[i].setAttribute("class", atr);
  }
}

document.querySelector("#tab-mv").addEventListener("click", () => {
  deactiveAll();
  document.querySelector("#form-mv").removeAttribute("class", "form-deactive");
  document.querySelector("#form-mv").setAttribute("class", "form-active");

  var atr = document.querySelector("#tab-mv").getAttribute("class") + " active";
  document.querySelector("#tab-mv").setAttribute("class", atr);
});

document.querySelector("#tab-tv").addEventListener("click", () => {
  deactiveAll();
  document.querySelector("#form-tv").removeAttribute("class", "form-deactive");
  document.querySelector("#form-tv").setAttribute("class","form-active");

  var atr = document.querySelector("#tab-tv").getAttribute("class") + " active";
  document.querySelector("#tab-tv").setAttribute("class", atr);
});

document.querySelector("#tab-pr").addEventListener("click", () => {
  deactiveAll();
  document.querySelector("#form-pr").removeAttribute("class", "form-deactive");
  document.querySelector("#form-pr").removeAttribute("class", "form-active");

  var atr = document.querySelector("#tab-pr").getAttribute("class") + " active";
  document.querySelector("#tab-pr").setAttribute("class", atr);
});
