# Registre de versions
Totts els canvis notables d'aquest projecte es registraran en aquest fitxer.


## [Unreleased] - yyyy-mm-dd
### Afegit
### Canviat
### Arreglat


## [1.3.1] - 2021-10-09

### Canviat
- Eliminat el footer


## [1.3.0] - 2021-09-27

### Afegit
- Pàgina de sel·lecció d'acció després de validació d'usuari
- Icona a la pestanya del navegador


## [1.1.29] - 2021-07-02

### Canviat
- Llicència canviada, de GPL a AGPL


## [1.1.28] - 2021-04-15

### Afegit
- Afegint validació del codi imdb a la pàgina d'inserció


## [1.1.27] - 2021-04-14

### Afegit
- Afegint el correu de contacte al peu de pàgina
