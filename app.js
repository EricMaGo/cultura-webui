require('dotenv').config(); //links with env file

const LocalStrategy  = require("passport-local"), //provides login
      User           = require("./models/user"),
      flash          = require("connect-flash"), //provides flash advertisements
      bodyParser     = require("body-parser"), //parses form response to body
      mongoose       = require("mongoose"), //database to save passport info
      passport       = require("passport"), //provides login
      express        = require("express"), //reders ejs files
      app = express();

const url = process.env.DATABASEURL || "mongodb://localhost/cultura"
const port = process.env.PORT || 8080;

mongoose.connect(url);

var selectRoutes = require("./routes/selectRoutes"),
    insertRoutes = require("./routes/insertRoutes"),
    indexRoutes = require("./routes/indexRoutes"),
    editRoutes = require("./routes/editRoutes");

app.use(bodyParser.urlencoded({extended: true})); //creates the body object in the post req
app.set("view engine", "ejs"); //allows the ejs files to execute
app.use(express.static(__dirname + '/public')); //allows to link files from the public folder to the ejs
app.use(flash()); //allows flash messages

//  ====================================
//          PASSPORT CONFIGURATION
//  ====================================

app.use(require("express-session")({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//  =======================================
//          END PASSPORT CONFIGURATION
//  =======================================

app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});

app.use("/", indexRoutes);
app.use("/select", selectRoutes);
app.use("/insert", insertRoutes);
app.use("/edit", editRoutes);

app.get("*", (req, res) => {
  res.render("notFound");
});

app.listen(port, process.env.IP, () => {
  if (process.env.PORT) {
    console.log("CulturaWUI started")
  } else {
    console.log("CulturaWUI started at localhost:" + port)
  }
});
