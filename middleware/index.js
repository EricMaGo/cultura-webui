var middlewareObj = {};

middlewareObj.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  req.flash("error", "Necessites perfil d'administrador per continuar");
  res.redirect("/login");
}
// middlewareObj.isLoggedIn = (req, res, next) => {
//   return next();
// }

module.exports = middlewareObj;
